INSTRUCCIONES:
¯¯¯¯¯¯¯¯¯¯¯¯¯¯
→  mueve la flecha de direccion en sentido horario.
←  mueve la flecha de direccion en sentido antihorario.
Barra Espaciadora  carga la barra espaciadora y dispara la
		   pelota.
Inicio  cancela el juego actual y vuelve al menu.
Fin  cierra el entorno y finaliza el juego.

Se recomienda instalar las fuentes de la carpeta fonts 
para que el juego se ejecute en su maximo explendor.